import React from "react";
import ReactDOM from "react-dom";
import App from "./App.jsx";
import { IntlProvider } from "react-intl";
import { ResourceProvider } from "./resource/ResourceProvider.jsx";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <React.StrictMode>
    <IntlProvider locale='en'>
      <ResourceProvider>
        <App/>
      </ResourceProvider>
    </IntlProvider>
  </React.StrictMode>,
  rootElement
);
