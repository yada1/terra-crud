import React from "react";
import "./styles.css";
import { Router, Route, Switch } from "react-router-dom";
import UserIndex from "./pages/UserIndex.jsx";
import UserEdit from "./pages/UserEdit.jsx";
import UserShow from "./pages/UserShow.jsx";
import UserCreate from "./pages/UserCreate.jsx";
import history from "./history.jsx";
import Error from "./components/Error";

export default function App() {
  return (
    <Router history={ history }>
      <Switch>
        <Route path="/" exact component={ UserIndex }/>
        <Route path="/users/new" exact component={ UserCreate }/>
        <Route path="/users/edit/:id" exact component={ UserEdit }/>
        <Route path="/users/:id" exact component={ UserShow }/>
        <Route path="/error" exact component={ Error }/>
      </Switch>
    </Router>
  );
}
