import React from 'react'
import UserDetails from "../components/UserDetails";
import Button from "terra-button";
import IconLeft from "terra-icon/lib/icon/IconLeft";
import { ResourceConsumer } from "../resource/ResourceProvider";
import { Link } from "react-router-dom";
import Header from "../components/Header";

const UserShow = () => {
  return (
    <ResourceConsumer>
      {
        value => {
          const { selectedUser } = value

          return (
            <>
              <Link to='/'>
                <Button
                  icon={ <IconLeft/> }
                  text='Back'
                  variant='neutral'
                  style={ { marginBottom: '50px' } }
                />
              </Link>

              <Header text='View Payer'/>

              <UserDetails selectedUser={ selectedUser }/>

              <Link to={ `/users/edit/${ selectedUser.id }` }>
                <Button
                  text='Edit'
                  variant='emphasis'
                  style={ { marginTop: '50px' } }
                />
              </Link>

            </>
          )
        }
      }
    </ResourceConsumer>
  )
}

export default UserShow