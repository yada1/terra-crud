import React from 'react'
import Header from "../components/Header";
import FinalForm from "../components/FinalForm";

const UserCreate = () => {
  return (
    <>
      <Header text='Add Payer'/>
      <FinalForm/>
    </>
  )
}

export default UserCreate