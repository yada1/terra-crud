import React from 'react'
import UserTable from "../components/UserTable.jsx";
import { ResourceConsumer } from "../resource/ResourceProvider.jsx";
import Header from "../components/Header";

const UserIndex = () => {
  return (
    <ResourceConsumer>
      {
        value => {
          const { users, filteredUsers, handleSelectedRow } = value

          return (
            <>
              <Header text='Payers'/>
              <UserTable
                users={ users }
                filteredUsers={ filteredUsers }
                handleSelectedRow={ handleSelectedRow }
              />
            </>
          )
        }
      }
    </ResourceConsumer>
  )
}

export default UserIndex