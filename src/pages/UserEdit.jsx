import React from 'react';
import { ResourceConsumer } from "../resource/ResourceProvider";
import Header from "../components/Header";
import FinalForm from "../components/FinalForm";

const UserEdit = () => {
  return (
    <ResourceConsumer>
      {
        value => {
          const { selectedUser, formForEdit } = value

          return (
            <>
              <Header text='Edit Payer'/>
              <FinalForm
                selectedUser={ selectedUser }
                formForEdit={ formForEdit }
              />
            </>
          )
        }
      }
    </ResourceConsumer>
  );
}

export default UserEdit;