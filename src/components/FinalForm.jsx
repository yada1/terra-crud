import React from 'react';
import { Form, Field } from 'react-final-form'
import '../styles.css'
import InputField from 'terra-form-input/lib/InputField';
import FormButtons from "./FormButtons";
import { ResourceConsumer } from "../resource/ResourceProvider";
import NotificationBanner from "./NotificationBanner";

const FinalForm = ({ formForEdit }) => {
  return (
    <ResourceConsumer>
      {
        value => {
          const {
            handleFormSubmit,
            identifyBtn,
            selectedUser,
            statusCode,
            btnClicked,
            clearStatusCode,
            updatedUser,
            createdUser
          } = value

          switch (statusCode) {
            case 201:
            case 200:
              return <NotificationBanner
                clearStatusCode={ clearStatusCode }
                btnClicked={ btnClicked }
                textMsg={ statusCode === 201
                  ? `A new payer has been added! ${createdUser.name} - ${createdUser.job}`
                  : `The record has been updated! ${updatedUser.name} - ${updatedUser.job}` }
              />
            case 204:
              return <NotificationBanner
                clearStatusCode={ clearStatusCode }
                btnClicked={ btnClicked }
                textMsg='The record has been deleted!'
              />
            default:
              break
          }

          const showForm = (handleSubmit) => (
            <form
              noValidate
              onSubmit={ handleSubmit }
            >
              <Field name="name">
                { ({ input, meta }) => (
                  <InputField
                    inputId="user-name"
                    label="Full Name"
                    error={ meta.error }
                    isInvalid={ meta.submitFailed && !meta.valid }
                    required
                    inputAttrs={ input }
                    onChange={ (e) => {
                      input.onChange(e.target.value);
                    } }
                    value={ input.value }
                  />
                ) }
              </Field>

              <Field name="job">
                { ({ input, meta }) => (
                  <InputField
                    inputId="user-job"
                    label="Job"
                    error={ meta.error }
                    isInvalid={ meta.submitFailed && !meta.valid }
                    required
                    inputAttrs={ input }
                    onChange={ (e) => {
                      input.onChange(e.target.value);
                    } }
                    value={ input.value }
                  />
                ) }
              </Field>

              <FormButtons
                formForEdit={ formForEdit }
                identifyBtn={ identifyBtn }
              />
            </form>
          )

          return (
            <>
              <h4 className='note'>Required fields are marked with an asterisk.</h4>

              <Form
                onSubmit={ handleFormSubmit }
                initialValues={ formForEdit ? {
                  name: `${ selectedUser.first_name } ${ selectedUser.last_name }`
                } : null }
                render={ ({ handleSubmit }) => showForm(handleSubmit) }
                validate={ (values) => {
                  const errors = {};
                  if (!values.name) {
                    errors.name = 'Required';
                  }
                  if (!values.job) {
                    errors.job = 'Required';
                  }
                  return errors;
                } }
              />
            </>
          )
        }
      }
    </ResourceConsumer>
  )
}

export default FinalForm;
