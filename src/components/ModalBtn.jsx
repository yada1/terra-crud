import React from 'react';
import { withDisclosureManager } from "terra-disclosure-manager";
import { disclosureType } from "terra-modal-manager";
import Button from "terra-button";
import ModalContents from "./ModalContents";

const ModalBtn = ({ disclosureManager }) => {
  return (
    <Button
      text="Delete"
      variant='emphasis'
      onClick={ () => {
        disclosureManager.disclose({
          preferredType: disclosureType,
          size: "tiny",
          content: {
            key: "my-modal-component-instance",
            component: <ModalContents/>
          }
        });
      } }
    />
  );
};

export default withDisclosureManager(ModalBtn);