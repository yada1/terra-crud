import React from 'react';
import Heading from "terra-heading";

const Header = ({ text }) => {
  return (
    <Heading level={ 1 } style={ { marginBottom: '20px' } }>{ text }</Heading>
  );
};

export default Header;