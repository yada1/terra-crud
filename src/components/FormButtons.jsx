import React from 'react';
import Button from "terra-button";
import { Link } from "react-router-dom";
import Modal from "./Modal";

const FormButtons = ({ formForEdit, identifyBtn }) => {
  if (formForEdit) {
    return (
      <div className='edit-form-buttons'>
        <Modal/>
        <div>
          <Link to='/'>
            <Button
              text="Cancel"
              variant='neutral'
              style={ { marginRight: '10px' } }
            />
          </Link>
          <Button
            id='saveE'
            text="Save"
            variant='emphasis'
            type={ Button.Opts.Types.SUBMIT }
            onClick={ identifyBtn }
          />
        </div>
      </div>
    )
  } else {
    return (
      <div className='create-form-buttons'>
        <Button
          id='saveC'
          text="Save"
          variant='emphasis'
          type={ Button.Opts.Types.SUBMIT }
          onClick={ identifyBtn }
        />
        <Button
          id='saveA'
          text="Save and Add Another"
          variant='emphasis'
          type={ Button.Opts.Types.SUBMIT }
          style={ { margin: '0 10px' } }
          onClick={ identifyBtn }
        />
        <Link to='/'>
          <Button
            text="Cancel"
            variant='neutral'
          />
        </Link>
      </div>
    )
  }
};

export default FormButtons;