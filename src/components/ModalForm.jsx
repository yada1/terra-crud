import React, { useState } from 'react';
import { Form, Field } from "react-final-form";
import InputField from 'terra-form-input/lib/InputField';
import Button from "terra-button";
import { ResourceConsumer } from "../resource/ResourceProvider";

const ModalForm = ({ disclosureManager }) => {
  const [fieldInput, setFieldInput] = useState('')

  return (
    <ResourceConsumer>
      {
        value => {
          const { handleFormSubmit, selectedUser, identifyBtn } = value
          const fullName = `${ selectedUser.first_name } ${ selectedUser.last_name }`

          const closeModal = () => {
            disclosureManager.dismiss().catch(() => {
              console.log("Dismiss failed. A lock must be in place.");
            })
          };

          const getFormInfo = values => {
            handleFormSubmit(values)
            closeModal()
          };

          const showModalForm = handleSubmit => (
            <form
              noValidate
              onSubmit={ handleSubmit }
            >
              <Field name="name">
                { ({ input, meta }) => (
                  <InputField
                    inputId="user-name"
                    error={ meta.error }
                    isInvalid={ fieldInput === '' ? meta.valid : !meta.valid }
                    onChange={ (e) => {
                      input.onChange(e.target.value)
                      setFieldInput(e.target.value)
                    } }
                    value={ input.value }
                  />
                ) }
              </Field>

              <div className='modal-form-buttons'>
                <Button
                  text="Cancel"
                  onClick={ closeModal }
                />
                <Button
                  id='delete'
                  text="Delete"
                  variant='emphasis'
                  isDisabled={ !(fieldInput === fullName) }
                  type={ Button.Opts.Types.SUBMIT }
                  onClick={ identifyBtn }
                />
              </div>
            </form>
          )

          return (
            <Form
              onSubmit={ getFormInfo }
              render={ ({ handleSubmit }) => showModalForm(handleSubmit) }
              validate={ (values) => {
                const errors = {};
                if (values.name !== fullName) {
                  errors.name = 'Input does not match resource name.';
                }
                return errors;
              } }
            />
          )
        }
      }
    </ResourceConsumer>
  );
};

export default ModalForm;