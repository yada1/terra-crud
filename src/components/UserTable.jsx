import Table from 'terra-table';
import React from "react";
import TableToolbar from "./TableToolbar.jsx";
import TablePaginator from "./TablePaginator.jsx";

const UserTable = ({ filteredUsers, handleSelectedRow, users }) => {
  function getUsers(users) {
    if (filteredUsers.length !== 0) {
      return filteredUsers
    } else {
      return users
    }
  }

  const renderUserList = () => getUsers(users)
    .map((
      user,
      index
    ) => {
      return {
        key: `row-${ index }`,
        isStriped: index % 2 === 1,
        cells: [
          { Key: `cell-0`, children: user.first_name },
          { Key: `cell-1`, children: user.last_name }
        ],
        toggleAction: {
          metaData: { id: `${ user.id }` },
          onToggle: handleSelectedRow
        }
      }
    });

  return (
    <Table
      summaryId="striped-table"
      summary="This table displays striped rows."
      rowStyle='toggle'
      headerNode={ <TableToolbar/> }
      footerNode={ <TablePaginator/> }
      numberOfColumns={ 2 }
      cellPaddingStyle="compact"
      dividerStyle="horizontal"
      headerData={ {
        cells: [
          { id: 'header-first_name', key: 'first_name', children: 'First Name' },
          { id: 'header-last_name', key: 'last_name', children: 'Last Name' },
        ],
      } }
      bodyData={ [
        {
          rows: renderUserList(),
        },
      ] }
    />
  )
}

export default UserTable
