import React from 'react';
import ModalManager from "terra-modal-manager";
import ModalBtn from "./ModalBtn";

const Modal = () => {
  return (
    <ModalManager>
      <ModalBtn/>
    </ModalManager>
  );
};

export default Modal;