import React from 'react';
import { ResourceConsumer } from "../resource/ResourceProvider";
import StatusView from 'terra-status-view';
import Button from "terra-button";
import history from "../history";

const Error = () => {
  return (
    <ResourceConsumer>
      {
        value => {
          const { errorMessage } = value

          return (
            <>
              <StatusView
                variant='error'
                title={ `${ errorMessage }` }
              />

              <div className='error-btn-wrapper'>
                <Button
                  text='Go Back!'
                  variant='emphasis'
                  onClick={ () => history.push('/') }
                />
              </div>
            </>
          )
        }
      }
    </ResourceConsumer>
  );
};

export default Error;