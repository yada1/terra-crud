import React from 'react';
import LabelValueView from 'terra-clinical-label-value-view';

const UserDetails = ({ selectedUser }) => {
  return (
    <LabelValueView className='label-value-view' label=''>
      <h3>Name</h3>
      <p>{ `${ selectedUser.first_name }  ${ selectedUser.last_name }` }</p>

      <h3>Email</h3>
      <p>{ `${ selectedUser.email }` }</p>

    </LabelValueView>
  );
};

export default UserDetails;