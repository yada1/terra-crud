import React from 'react';
import ProgressivePaginator from 'terra-paginator/lib/ProgressivePaginator';
import { ResourceConsumer } from "../resource/ResourceProvider.jsx";
import '../styles.css'

const TablePaginator = () => {
  return (
    <ResourceConsumer>
      {
        value => {
          const { currentPage, totalCount, countPerPage, handleNextPagination } = value

          return (
            <ProgressivePaginator
              onPageChange={ handleNextPagination }
              selectedPage={ currentPage }
              totalCount={ totalCount }
              itemCountPerPage={ countPerPage }
            />
          )
        }
      }
    </ResourceConsumer>
  );
};

export default TablePaginator;
