import React from 'react';
import IconAdd from 'terra-icon/lib/icon/IconAdd'
import Button from "terra-button";
import Checkbox from 'terra-form-checkbox';
import SearchField from "terra-search-field";
import { Link } from "react-router-dom";
import { ResourceConsumer } from "../resource/ResourceProvider.jsx";

const TableToolbar = () => {
  return (
    <ResourceConsumer>
      {
        value => {
          const { handleSearch } = value

          return (
            <div className='table-toolbar'>
              <Link
                to='/users/new'
                className='add-sign'
              >
                <Button
                  icon={ <IconAdd/> }
                  text='Add'
                  variant='ghost'
                />
              </Link>
              <div className='check-box-and-search'>
                <Checkbox
                  id='activeOnly'
                  labelText='Active Only'
                  defaultChecked
                />&nbsp;&nbsp;&nbsp;&nbsp;
                <SearchField
                  minimumSearchTextLength={ 0 }
                  placeholder='Search'
                  onSearch={ handleSearch }
                />
              </div>
            </div>
          )
        }
      }
    </ResourceConsumer>
  )
}

export default TableToolbar