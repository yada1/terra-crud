import React from "react";
import {
  withDisclosureManager,
  DisclosureManagerHeaderAdapter
} from "terra-disclosure-manager";
import Spacer from "terra-spacer";
import Heading from "terra-heading";
import Text from "terra-text";
import { ResourceConsumer } from "../resource/ResourceProvider";
import ModalForm from "./ModalForm";

const ModalContents = ({ disclosureManager }) => (
  <ResourceConsumer>
    {
      value => {
        const { selectedUser } = value

        return (
          <div>
            <DisclosureManagerHeaderAdapter title="CONFIRM DELETE"/>
            <Spacer padding="large">
              <Heading level={ 3 }>
                Are you sure you want to delete resource:
                { `${ ' ' + selectedUser.first_name } ${ selectedUser.last_name }` }?
              </Heading>
              <Spacer marginTop='large'/>
              <Text>Enter the resource name to continue.</Text>
              <ModalForm
                disclosureManager={ disclosureManager }
                selectedUser={ selectedUser }
              />
            </Spacer>
          </div>
        )
      }
    }
  </ResourceConsumer>
);

export default withDisclosureManager(ModalContents);
