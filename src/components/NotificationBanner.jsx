import React, { useState } from 'react';
import NotificationDialog from 'terra-notification-dialog';
import history from "../history";

const NotificationBanner = ({ btnClicked, textMsg, clearStatusCode }) => {
  const [isOpen, setIsOpen] = useState(true)

  const handleCloseModal = () => {
    if (btnClicked === 'saveC' || btnClicked === 'saveE' || btnClicked === 'delete') {
      clearStatusCode()
      setIsOpen(false)
      history.push('/')
    } else if (btnClicked === 'saveA') {
      window.location.reload()
    }
  };

  return (
    <>
      { isOpen && (
        <NotificationDialog
          variant="hazard-low"
          dialogTitle='Success!'
          startMessage={ textMsg }
          acceptAction={ {
            text: 'OK',
            onClick: handleCloseModal
          } }
        />
      ) }
    </>
  );
};

export default NotificationBanner;