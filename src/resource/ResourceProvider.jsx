import React   from 'react';
import history from "../history.jsx";
import _       from 'lodash'

const ResourceContext = React.createContext()
const ResourceConsumer = ResourceContext.Consumer
const regresUrl = 'https://reqres.in/api/users'

class ResourceProvider extends React.Component {
  state = {
    users: [],
    searchText: '',
    filteredUsers: [],
    selectedUser: {},
    updatedUser: {},
    createdUser: {},
    totalCount: null,
    countPerPage: null,
    currentPage: 1,
    errorMessage: '',
    btnClicked: '',
    formForEdit: true,
    statusCode: null
  }

  componentDidMount() {
    this.getResource(this.state.currentPage)
      .then(returnedData => {
        this.setUpState(returnedData)
        this.sortUsers()
      })
  }

  setUpState = data => {
    this.setState({
      ...this.state,
      users: data.data,
      totalCount: data.total,
      countPerPage: data.per_page
    })
  }

  getResource = async (page) => {
    try {
      const response = await fetch(`${regresUrl}?page=${ page }`)
      return await response.json()
    } catch (error) {
      console.log(error)
    }
  }

  getUser = async id => {
    try {
      const response = await fetch(`${regresUrl}/${id}`)
      return await response.json();
    } catch (error) {
      console.log(error)
    }
  }

  sortUsers = () => {
    const tempUsers = this.state.users
    tempUsers.sort((x, y) => {
      let a = x.first_name, b = y.first_name;
      return a === b ? 0 : a > b ? 1 : -1;
    })
    this.setState({ users: tempUsers })
  }

  handleSearch = (inputText) => {
    this.setState({ searchText: inputText }, this.findSearchText)
  }

  findSearchText = () => {
    const { searchText, users } = this.state
    let tempUsers = [...users]

    if (searchText.length > 0) {
      // eslint-disable-next-line array-callback-return
      tempUsers = tempUsers.filter(user => {
        let tempSearchText = searchText.toLowerCase()
        let tempName = user.first_name.toLowerCase().slice(0, searchText.length)
        if (tempSearchText === tempName) {
          return user
        }
      })
    } else if (searchText.length === 0) {
      tempUsers = []
    }

    this.setState({ filteredUsers: tempUsers })
  }

  handleSelectedRow = (e, metaData) => {
    e.preventDefault()
    this.getUser(metaData.id)
      .then(returnedData => {
        if (_.isEmpty(returnedData)) {
          this.setState({ errorMessage: '404, the user is not found!' })
          history.push('/error')
        } else {
          this.setState({ selectedUser: returnedData.data })
          history.push(`/users/${ metaData.id }`)
        }
      })
  }

  handleNextPagination = (pageNum) => {
    this.getResource(pageNum)
        .then(returnedData => {
          this.setUpState(returnedData)
          this.sortUsers()
        })
    this.setState({ currentPage: pageNum })
  }

  updateRecord = async (formInfo) => {
    try {
      const response = await fetch(`${regresUrl}/${ this.state.selectedUser.id }`, {
        method: 'PUT',
        body: JSON.stringify(formInfo),
        headers: {
          'Content-type': 'application/json; charset=UTF-8'
        }
      })
      this.setState({ statusCode: response.status })
      return await response.json()
    } catch (error) {
      console.log(error)
    }
  }

  createRecord = async (formInfo) => {
    try {
      const response = await fetch(`${regresUrl}`, {
        method: 'POST',
        body: JSON.stringify(formInfo),
        headers: {
          'Content-type': 'application/json'
        }
      })
      this.setState({ statusCode: response.status })
      return await response.json()
    } catch (error) {
      console.log(error)
    }
  }

  deleteRecord = async () => {
    try {
      return await fetch(`${ regresUrl }/${ this.state.selectedUser.id }`, {
        method: 'DELETE'
      })
    } catch (error) {
      console.log(error)
    }
  }

  handleFormSubmit = (values) => {
    if (this.state.btnClicked === 'saveE') {
      this.updateRecord(values)
        .then(returnedData => {
          this.setState({updatedUser: returnedData})
        })
    } else if (this.state.btnClicked === 'saveC' || this.state.btnClicked === 'saveA') {
      this.createRecord(values)
        .then(returnedData => {
          this.setState({createdUser: returnedData})
        })
    } else {
      this.deleteRecord()
        .then(returnedData => {
          this.setState({ statusCode: returnedData.status })
      })
    }
  }

  identifyBtn = (e) => {
    this.setState({ btnClicked: e.currentTarget.id })
  }

  clearStatusCode = () => {
    this.setState({ statusCode: null })
  }

  render() {
    if (this.state.users.length === 0) {
      return <div>Loading...</div>
    }

    return (
      <ResourceContext.Provider
        value={ {
          ...this.state,
          handleSearch: this.handleSearch,
          handleSelectedRow: this.handleSelectedRow,
          handleNextPagination: this.handleNextPagination,
          handleFormSubmit: this.handleFormSubmit,
          identifyBtn: this.identifyBtn,
          clearStatusCode: this.clearStatusCode
        } }
      >
        { this.props.children }
      </ResourceContext.Provider>
    )
  }
}

export { ResourceProvider, ResourceConsumer }